﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlinePizzaDelivery.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Pizzat",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Emri = table.Column<string>(type: "nvarchar(30)", nullable: false),
                    Perberesit = table.Column<string>(type: "varchar(200)", nullable: true),
                    Madhesia = table.Column<string>(type: "varchar(100)", nullable: true),
                    Cmimi = table.Column<decimal>(type: "decimal(10,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pizzat", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Pizzat");
        }
    }
}
