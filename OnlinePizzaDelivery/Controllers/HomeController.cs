﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OnlinePizzaDelivery.Data;
using OnlinePizzaDelivery.Models;

namespace OnlinePizzaDelivery.Controllers
{

    public class HomeController : Controller
    {
        private ApplicationDbContext _dbContext;
        public HomeController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }


        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [Authorize]
        public IActionResult Products()
        {
            var pizzas = _dbContext.Pizzat.ToList();

            var vm = new IndexViewModel
            {
                Pizzas = pizzas
            };

            return View(vm);
        }
    }
}
