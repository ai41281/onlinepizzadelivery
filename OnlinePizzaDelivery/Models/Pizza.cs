﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlinePizzaDelivery.Models
{
    public class Pizza
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        [Required(ErrorMessage = "Kjo fushë është e nevojshme.")]
        [DisplayName("Emri")]
        public string Emri { get; set; }

        [Column(TypeName = "varchar(200)")]
        [DisplayName("Përbërësit")]
        public string Perberesit { get; set; }

        [Column(TypeName = "varchar(100)")]
        [DisplayName("Madhësia")]
        public string Madhesia { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        [DisplayName("Cmimi")]
        public double Cmimi { get; set; }
    }
}
