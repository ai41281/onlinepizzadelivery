﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlinePizzaDelivery.Models
{
    public class IndexViewModel
    {
        public List<Pizza> Pizzas { get; set; }
    }
}
