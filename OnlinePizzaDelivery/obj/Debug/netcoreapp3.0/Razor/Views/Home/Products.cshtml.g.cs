#pragma checksum "C:\Users\DELL\Downloads\OnlinePizzaDelivery2\Views\Home\Products.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "08bc8292aea5c35a48c0a6ca64c980fa793fdc4e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Products), @"mvc.1.0.view", @"/Views/Home/Products.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\DELL\Downloads\OnlinePizzaDelivery2\Views\_ViewImports.cshtml"
using OnlinePizzaDelivery;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\DELL\Downloads\OnlinePizzaDelivery2\Views\_ViewImports.cshtml"
using OnlinePizzaDelivery.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"08bc8292aea5c35a48c0a6ca64c980fa793fdc4e", @"/Views/Home/Products.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fb3edbfdd82ab00adfc395b8cbe8d8113e1229c9", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Products : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IndexViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\DELL\Downloads\OnlinePizzaDelivery2\Views\Home\Products.cshtml"
  
    ViewBag.Title = "Home";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<h2 class=""text-center"">PRODUKTET</h2>
<table class=""table table-hover"">
    <thead>
        <tr>
            <th>
                Emri
            </th>
            <th>
                Përbërësit
            </th>
            <th>
                Madhësia
            </th>
            <th>
                Cmimi
            </th>

        </tr>
    </thead>
    <tbody>
");
#nullable restore
#line 25 "C:\Users\DELL\Downloads\OnlinePizzaDelivery2\Views\Home\Products.cshtml"
         foreach (var item in Model.Pizzas)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n                <td>\r\n\r\n                    ");
#nullable restore
#line 30 "C:\Users\DELL\Downloads\OnlinePizzaDelivery2\Views\Home\Products.cshtml"
               Write(item.Emri);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>");
#nullable restore
#line 32 "C:\Users\DELL\Downloads\OnlinePizzaDelivery2\Views\Home\Products.cshtml"
               Write(item.Perberesit);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>\r\n                    ");
#nullable restore
#line 34 "C:\Users\DELL\Downloads\OnlinePizzaDelivery2\Views\Home\Products.cshtml"
               Write(item.Madhesia);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>");
#nullable restore
#line 36 "C:\Users\DELL\Downloads\OnlinePizzaDelivery2\Views\Home\Products.cshtml"
               Write(item.Cmimi);

#line default
#line hidden
#nullable disable
            WriteLiteral(" €</td>\r\n\r\n            </tr>\r\n");
#nullable restore
#line 39 "C:\Users\DELL\Downloads\OnlinePizzaDelivery2\Views\Home\Products.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        \r\n    </tbody>\r\n</table>\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IndexViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
