﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OPD.Data;
using OPD.Models;

namespace OPD.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class OrdersController : Controller
    {
        private readonly ApplicationDbContext _context;
      
        public OrdersController(ApplicationDbContext context)
        {
            _context = context;
          
        }

        [HttpGet]
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {
            ViewData["Data"] = String.IsNullOrEmpty(sortOrder) ? "data_desc" : "";
            ViewData["CurrentFilter"] = searchString;
            ViewData["CurrentSort"] = sortOrder;
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else if (pageNumber < 1)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var order = from p in _context.Orders
                        select p;
            if (!String.IsNullOrEmpty(searchString))
            {
                order = order.Where(s => s.ProdEmri.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "data_desc":
                    order = order.OrderByDescending(s => s.Data);
                    break;
                default:
                    order = order.OrderBy(s => s.Data);
                    break;
            }

            int pageSize = 9;
            return View(await PaginatedList<Order>.CreateAsync(order.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        public async Task<IActionResult> Confirm(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            return View(order);
        }
        // POST: Patients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Confirm(int id, [Bind("Id,ProductId,UserId,Cmimi,Emri,Imgname,Sasia,Data,OrderId,ProdEmri,PhoneNumber,Statusi,Adresa")] Order order)
        {
            
            if (id != order.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
              try
                {
                    
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                   
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                
            }
            return RedirectToAction("Index", "Orders");
        }
        private bool OrderExists(int id)
        {
            return _context.Orders.Any(e => e.Id == id);
        }

    }
}