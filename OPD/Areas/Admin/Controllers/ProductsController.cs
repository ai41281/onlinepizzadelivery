﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OPD.Data;
using OPD.Models;

namespace OPD.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class ProductsController : Controller
    {
       
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _iwebhost;
        public ProductsController(ApplicationDbContext context, IWebHostEnvironment iwebhost)
        {
            _context = context;
            _iwebhost = iwebhost;
        }

        [HttpGet]
        // GET: Admin/Products
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {
            ViewData["Emri"] = String.IsNullOrEmpty(sortOrder) ? "emri_desc" : "";
            ViewData["CurrentFilter"] = searchString;
            var produktet = from p in _context.Produktet
                           select p;
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else if(pageNumber < 1)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                produktet = produktet.Where(s => s.Emri.Contains(searchString)
                                       || s.Perberesit.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "emri_desc":
                    produktet = produktet.OrderByDescending(s => s.Emri);
                    break;
                
                default:
                    produktet = produktet.OrderBy(s => s.Emri);
                    break;
            }
            int pageSize = 5;
            return View(await PaginatedList<Product>.CreateAsync(produktet.AsNoTracking(), pageNumber ?? 1, pageSize));
            
        }

        // GET: Admin/Products/Details/5
        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Produktet
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }





        // GET: Admin/Products/Create
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(IFormFile ifile, Product ic)
        {
            if (ifile != null)
            {
                string imgext = Path.GetExtension(ifile.FileName);
                if (imgext == ".jpg" || imgext == ".png")
                {

                    var saveimg = Path.Combine(_iwebhost.WebRootPath, "Images", ifile.FileName);
                    var stream = new FileStream(saveimg, FileMode.Create);
                    await ifile.CopyToAsync(stream);
                    ic.Imgname = ifile.FileName;
                    ic.Imgpath = saveimg;

                    await _context.Produktet.AddAsync(ic);
                    await _context.SaveChangesAsync();
                    ViewData["Message"] = "The selected image is saved";
                    stream.Close();
                }
            }
            else
            {
                ViewData["Message"] = "Error";

            }
            return RedirectToAction("Index");
        }

        // GET: Admin/Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Produktet.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

        // POST: Admin/Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ProductId,Imgname,Imgpath,Emri,Perberesit,Cmimi")] Product product)
        {
            if (id != product.ProductId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(product);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.ProductId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Admin/Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Produktet
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Admin/Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var product = await _context.Produktet.FindAsync(id);
            _context.Produktet.Remove(product);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductExists(int id)
        {
            return _context.Produktet.Any(e => e.ProductId == id);
        }
    }
}
