﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OPD.Data;
using OPD.Models;

namespace OPD.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {


        private readonly ApplicationDbContext _context;
  

        private readonly UserManager<ApplicationUser> userManager;
        public UsersController(UserManager<ApplicationUser> userManager, ApplicationDbContext context)
        {
            this.userManager = userManager;
            _context = context;
        }
        [HttpGet]
        public async Task<IActionResult>Index(string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {
            ViewData["Emri"] = String.IsNullOrEmpty(sortOrder) ? "emri_desc" : "";
            ViewData["CurrentFilter"] = searchString;
            var users = from p in _context.Users
                            select p;
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else if (pageNumber < 1)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                users = users.Where(s => s.Emri.Contains(searchString)
                                       || s.Email.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "emri_desc":
                    users = users.OrderByDescending(s => s.Emri);
                    break;

                default:
                    users = users.OrderBy(s => s.Emri);
                    break;
            }
            int pageSize = 6;
            return View(await PaginatedList<ApplicationUser>.CreateAsync(users.AsNoTracking(), pageNumber ?? 1, pageSize));

           
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            var user = await userManager.FindByIdAsync(id);

            if (user == null)
            {
                ViewBag.ErrorMessage = $"User with Id = {id} cannot be found";
                return View("NotFound");
            }
            else
            {
                var result = await userManager.DeleteAsync(user);

                if (result.Succeeded)
                {
                    return RedirectToAction();
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }

                return View();
            }
        }




    }
}