﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OPD.Data.Migrations
{
    public partial class Price : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "Cmimi",
                table: "Produktet",
                maxLength: 12,
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float",
                oldMaxLength: 12);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Cmimi",
                table: "Produktet",
                type: "float",
                maxLength: 12,
                nullable: false,
                oldClrType: typeof(float),
                oldMaxLength: 12);
        }
    }
}
