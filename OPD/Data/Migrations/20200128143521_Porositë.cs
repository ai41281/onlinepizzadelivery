﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OPD.Data.Migrations
{
    public partial class Porositë : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "Cmimi",
                table: "Orders",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<string>(
                name: "Emri",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Imgname",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Sasia",
                table: "Orders",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Cmimi",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "Emri",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "Imgname",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "Sasia",
                table: "Orders");
        }
    }
}
