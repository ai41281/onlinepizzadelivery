﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OPD.Data.Migrations
{
    public partial class Produktet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Produktet",
                columns: table => new
                {
                    ProductId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Imgname = table.Column<string>(nullable: false),
                    Imgpath = table.Column<string>(nullable: false),
                    Emri = table.Column<string>(nullable: true),
                    Perberesit = table.Column<string>(nullable: true),
                    Cmimi = table.Column<float>(nullable: false),
                    ProductId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Produktet", x => x.ProductId);
                    table.ForeignKey(
                        name: "FK_Produktet_Produktet_ProductId1",
                        column: x => x.ProductId1,
                        principalTable: "Produktet",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Produktet_ProductId1",
                table: "Produktet",
                column: "ProductId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Produktet");
        }
    }
}
