﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OPD.Data.Migrations
{
    public partial class Pricee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Cmimi",
                table: "Produktet",
                nullable: false,
                oldClrType: typeof(float),
                oldType: "real",
                oldMaxLength: 12);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "Cmimi",
                table: "Produktet",
                type: "real",
                maxLength: 12,
                nullable: false,
                oldClrType: typeof(int));
        }
    }
}
