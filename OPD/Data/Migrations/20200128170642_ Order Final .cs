﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OPD.Data.Migrations
{
    public partial class OrderFinal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProdEmri",
                table: "Orders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProdEmri",
                table: "Orders");
        }
    }
}
