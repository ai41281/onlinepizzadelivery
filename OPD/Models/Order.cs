﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OPD.Models
{
    public class Order
    {
       
        public int Id { get; set; }
        public int OrderId { get; set; }
        [ForeignKey("Product")]
        [ReadOnly(true)]
        public int ProductId { get; set; }

        [ForeignKey("User")]
        [ReadOnly(true)]
        public string UserId { get; set; }
        [ReadOnly(true)]
        public string Imgname { get; set; }
        public string ProdEmri { get; set; }
        [ReadOnly(true)]
        public string Emri { get; set; }
        public int Sasia { get; set; }
        [ReadOnly(true)]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Vetëm numra")]
        public float Cmimi { get; set; }
      
        public DateTime? Data { get; set; }
        Random r = new Random();
        public Order()
        {
            this.Data = DateTime.UtcNow;
            this.OrderId = r.Next();
        }
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Vetëm shkronja")]
        public string Adresa { get; set; }
        public bool? Statusi { get; set; }
        public string PhoneNumber { get; set; }
    }
}
