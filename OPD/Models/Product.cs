﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OPD.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        [Required]
        public string Imgname { get; set; }
        [Required]
        public string Imgpath { get; set; }
        [Required]

        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Vetëm shkronja")]
        public string Emri { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z]+[ a-zA-Z-_]*$", ErrorMessage = "Vetëm shkronja")]
        public string Perberesit { get; set; }
        [Required]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Vetëm numra")]
        public int Cmimi { get; set; }

        public DbSet<Product> Produktet { get; set; }
    }
}
