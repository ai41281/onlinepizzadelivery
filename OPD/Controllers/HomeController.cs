﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OPD.Data;
using OPD.Models;

namespace OPD.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;
        public HomeController(ApplicationDbContext context, ILogger<HomeController> logger, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            
            return View();
        }

        
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Produktet(string sortOrder,
    string currentFilter,
    string searchString,
    int? pageNumber)
        {
            ViewData["Emri"] = String.IsNullOrEmpty(sortOrder) ? "emri_desc" : "";
            ViewData["CurrentFilter"] = searchString;
            ViewData["CurrentSort"] = sortOrder;
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else if (pageNumber < 1)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var produktet = from p in _context.Produktet
                            select p;
            if (!String.IsNullOrEmpty(searchString))
            {
                produktet = produktet.Where(s => s.Emri.Contains(searchString)
                                       || s.Perberesit.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "emri_desc":
                    produktet = produktet.OrderByDescending(s => s.Emri);
                    break;
                default:
                    produktet = produktet.OrderBy(s => s.Emri);
                    break;
            }

            int pageSize = 9;
            return View(await PaginatedList<Product>.CreateAsync(produktet.AsNoTracking(), pageNumber ?? 1, pageSize));
        }
        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            ViewBag.userId = _userManager.GetUserId(HttpContext.User);
            ViewBag.Emri = _userManager.GetUserAsync(User).Result.Emri;
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Produktet
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }
       

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
