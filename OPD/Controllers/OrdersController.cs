﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OPD.Data;
using OPD.Models;

namespace OPD.Controllers
{
    public class OrdersController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
      
        private readonly ApplicationDbContext _context;
        public OrdersController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Buy(Order order)
        {
            ViewBag.userId = _userManager.GetUserId(HttpContext.User);
            ViewBag.Emri = _userManager.GetUserAsync(User).Result.Emri;
            await _context.Orders.AddAsync(order);
            await _context.SaveChangesAsync();

            return RedirectToAction("MyOrders", "Orders");
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> MyOrders(string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {
            ViewBag.userId = _userManager.GetUserId(HttpContext.User);
            ViewData["Data"] = String.IsNullOrEmpty(sortOrder) ? "data_desc" : "";
            ViewData["CurrentFilter"] = searchString;
            ViewData["CurrentSort"] = sortOrder;
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else if (pageNumber < 1)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var order = from p in _context.Orders.Where(p => p.UserId == _userManager.GetUserId(HttpContext.User))
                        select p;
            if (!String.IsNullOrEmpty(searchString))
            {
                order = order.Where(s => s.ProdEmri.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "data_desc":
                    order = order.OrderByDescending(s => s.Data);
                    break;
                default:
                    order = order.OrderBy(s => s.Data);
                    break;
            }

            int pageSize = 9;
            return View(await PaginatedList<Order>.CreateAsync(order.AsNoTracking(), pageNumber ?? 1, pageSize));
        }
        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Orders
                .FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var order = await _context.Orders.FindAsync(id);
            if(order.Statusi == null)
            {
                _context.Orders.Remove(order);
                await _context.SaveChangesAsync();
                return RedirectToAction("MyOrders", "Orders");
            }
            else
            {
                return RedirectToAction("MyOrders", "Orders");
            }
           
        }
        [Authorize]
        public async Task<IActionResult> OrderDetails(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders
                .FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            return View(order);
        }

        // POST: Patients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProductId,UserId,Cmimi,Emri,Imgname,Sasia,Data,OrderId,ProdEmri,PhoneNumber,Statusi,Adresa")] Order order)
        {
            if (id != order.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("MyOrders", "Orders");
            }
            return View(order);
        }

        private bool OrderExists(int id)
        {
            return _context.Orders.Any(e => e.Id == id);
        }




    }
}